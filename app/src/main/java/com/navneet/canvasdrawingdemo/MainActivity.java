package com.navneet.canvasdrawingdemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Setup your system for drawing ( get your tools ready!)

        // 1. Setup the frame
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap b = Bitmap.createBitmap(300, 500, Bitmap.Config.ARGB_8888);

        //2. Setup the canvas
        Canvas canvas = new Canvas(b);

        //3. Setup your paintbrush
        Paint paintBrush = new Paint();


        //Draw some stuff

        //4. Add the code to start painting

        //i. Set the background color
        canvas.drawColor(Color.BLACK);

        //ii. Choose a Yellow Crayon
        paintBrush.setColor(Color.YELLOW);

        // Draw the line
        canvas.drawLine(10, 50, 200, 50, paintBrush);


        //iii. Choose a Pink Crayon
        paintBrush.setColor(Color.YELLOW);

//        iv) Draw Another Diagonal Line
        canvas.drawLine(10, 50, 200, 5, paintBrush);


        //Draw some sqaures

        //DRaw 20 X 20 sqaure
        paintBrush.setColor(Color.WHITE);
        canvas.drawRect(100, 100, 120, 120, paintBrush);

        //Draw a 50 X 50 sqaures
        paintBrush.setColor(Color.GREEN);
        canvas.drawRect(150, 150, 200, 200, paintBrush);


        //Draw Text


        //1. Set the text size
        paintBrush.setTextSize(40);
        paintBrush.setColor(Color.WHITE);


        //2. Draw text onto screen
        canvas.drawText("Hello World!", 10, 400, paintBrush);

        //3. Draw some more text
        paintBrush.setTextSize(10);

        canvas.drawText("GoodBye World!", 10, 450, paintBrush);


        //Put the canvas into Frame
        imageView.setImageBitmap(b);

    }
}
